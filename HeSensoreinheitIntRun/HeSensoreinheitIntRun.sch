EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Helium system sensor unit"
Date "2021-02-19"
Rev "v1.0"
Comp "Paul Scherrer Institut"
Comment1 "WBWA/131 Tel. 3671"
Comment2 "Frank Meier"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L HeSensor:DPS310brd U6
U 1 1 602FD997
P 6200 2900
F 0 "U6" H 6342 2385 50  0000 C CNN
F 1 "DPS310brd" H 6342 2476 50  0000 C CNN
F 2 "" H 6500 2850 50  0001 C CNN
F 3 "" H 6500 2850 50  0001 C CNN
	1    6200 2900
	-1   0    0    1   
$EndComp
$Comp
L HeSensor:ADS1115brd U4
U 1 1 602FDF6A
P 5800 4200
F 0 "U4" H 6278 4246 50  0000 L CNN
F 1 "ADS1115brd" H 6278 4155 50  0000 L CNN
F 2 "" H 6100 3600 50  0001 C CNN
F 3 "" H 6100 3600 50  0001 C CNN
	1    5800 4200
	1    0    0    -1  
$EndComp
$Comp
L HeSensor:MAX31856brd U5
U 1 1 602FE22F
P 5950 1650
F 0 "U5" H 5900 1035 50  0000 C CNN
F 1 "MAX31856brd" H 5900 1126 50  0000 C CNN
F 2 "" H 6250 1700 50  0001 C CNN
F 3 "" H 6250 1700 50  0001 C CNN
	1    5950 1650
	-1   0    0    1   
$EndComp
$Comp
L Sensor_Temperature:LM35-LP U3
U 1 1 602FED89
P 3450 6350
F 0 "U3" H 3121 6396 50  0000 R CNN
F 1 "LM35-LP" H 3121 6305 50  0000 R CNN
F 2 "" H 3500 6100 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 3450 6350 50  0001 C CNN
	1    3450 6350
	1    0    0    -1  
$EndComp
$Comp
L HeSensor:O2-A2 U1
U 1 1 603019F2
P 3450 4400
F 0 "U1" H 3507 4767 50  0000 C CNN
F 1 "O2-A2" H 3507 4676 50  0000 C CNN
F 2 "" H 3900 4400 50  0001 C CNN
F 3 "" H 3900 4400 50  0001 C CNN
	1    3450 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60302161
P 4850 4400
F 0 "R1" H 4920 4446 50  0000 L CNN
F 1 "100" H 4920 4355 50  0000 L CNN
F 2 "" V 4780 4400 50  0001 C CNN
F 3 "~" H 4850 4400 50  0001 C CNN
	1    4850 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:Thermocouple TC1
U 1 1 6030313C
P 3450 1650
F 0 "TC1" H 3253 1626 50  0000 R CNN
F 1 "Thermocouple" H 3253 1717 50  0000 R CNN
F 2 "" H 2875 1700 50  0001 C CNN
F 3 "~" H 2875 1700 50  0001 C CNN
	1    3450 1650
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 6030CDFA
P 9250 2700
F 0 "J1" H 9330 2692 50  0000 L CNN
F 1 "LEMO HGG.2B 16 pin" H 9330 2601 50  0000 L CNN
F 2 "" H 9250 2700 50  0001 C CNN
F 3 "~" H 9250 2700 50  0001 C CNN
	1    9250 2700
	1    0    0    -1  
$EndComp
Text GLabel 9050 2000 0    50   Input ~ 0
+5V
Text GLabel 9050 2100 0    50   Input ~ 0
GND
Text GLabel 6300 3000 2    50   Input ~ 0
GND
Text GLabel 3450 6650 3    50   Input ~ 0
GNDA
Text GLabel 3450 6050 1    50   Input ~ 0
+5VA
Text GLabel 3450 4950 1    50   Input ~ 0
+5VA
Text GLabel 3300 5650 0    50   Input ~ 0
GNDA
Wire Wire Line
	5150 4350 5150 4250
Wire Wire Line
	5150 4250 4850 4250
Wire Wire Line
	5150 4350 5700 4350
Wire Wire Line
	5700 4450 5150 4450
Wire Wire Line
	5150 4450 5150 4550
Wire Wire Line
	5150 4550 4850 4550
$Comp
L HeSensor:HIH-4010 U2
U 1 1 60301415
P 3450 5250
F 0 "U2" H 3121 5296 50  0000 R CNN
F 1 "HIH-4010" H 3121 5205 50  0000 R CNN
F 2 "" H 3450 5250 50  0001 C CNN
F 3 "" H 3450 5250 50  0001 C CNN
	1    3450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4250 4100 4300
Wire Wire Line
	4100 4300 3850 4300
Wire Wire Line
	3850 4500 4100 4500
Wire Wire Line
	4100 4500 4100 4550
Wire Wire Line
	5300 5250 5300 4550
Wire Wire Line
	5300 4550 5700 4550
Wire Wire Line
	3850 6350 5450 6350
Wire Wire Line
	5450 6350 5450 4650
Wire Wire Line
	5450 4650 5700 4650
Wire Wire Line
	4850 4250 4100 4250
Connection ~ 4850 4250
Wire Wire Line
	4100 4550 4850 4550
Connection ~ 4850 4550
NoConn ~ 6300 1950
NoConn ~ 6300 3100
NoConn ~ 6300 1350
NoConn ~ 6300 1250
NoConn ~ 5700 4250
NoConn ~ 5700 4150
Text GLabel 5700 3750 0    50   Input ~ 0
+5V
Text GLabel 5700 3850 0    50   Input ~ 0
GND
Text GLabel 9050 2200 0    50   Input ~ 0
SDI
Text GLabel 9050 2300 0    50   Input ~ 0
SDO
Text GLabel 9050 2400 0    50   Input ~ 0
SCK
Text GLabel 6300 1550 2    50   Input ~ 0
SDI
Text GLabel 6300 1650 2    50   Input ~ 0
SDO
Text GLabel 6300 1750 2    50   Input ~ 0
SCK
Text GLabel 6300 2700 2    50   Input ~ 0
SDI
Text GLabel 6300 2800 2    50   Input ~ 0
SDO
Text GLabel 6300 2900 2    50   Input ~ 0
SCK
Wire Wire Line
	8050 2500 9050 2500
Wire Wire Line
	6300 2600 9050 2600
Text GLabel 6300 2050 2    50   Input ~ 0
+5V
Text GLabel 6300 1850 2    50   Input ~ 0
GND
Text GLabel 9050 2700 0    50   Input ~ 0
SCL
Text GLabel 9050 2800 0    50   Input ~ 0
SDA
Text GLabel 5700 3950 0    50   Input ~ 0
SCL
Text GLabel 5700 4050 0    50   Input ~ 0
SDA
Wire Wire Line
	6300 1450 8050 1450
Wire Wire Line
	8050 1450 8050 2500
NoConn ~ 9050 3100
NoConn ~ 9050 3200
NoConn ~ 9050 3300
NoConn ~ 9050 3400
NoConn ~ 9050 3500
Text GLabel 6300 3200 2    50   Input ~ 0
+5V
Wire Wire Line
	5700 1600 3750 1600
Wire Wire Line
	3750 1600 3750 1550
Wire Wire Line
	3750 1550 3550 1550
Wire Wire Line
	5700 1700 3750 1700
Wire Wire Line
	3750 1700 3750 1750
Wire Wire Line
	3750 1750 3550 1750
$Comp
L Device:R R2
U 1 1 6032A82E
P 4150 5450
F 0 "R2" H 4220 5496 50  0000 L CNN
F 1 "80k" H 4220 5405 50  0000 L CNN
F 2 "" V 4080 5450 50  0001 C CNN
F 3 "~" H 4150 5450 50  0001 C CNN
	1    4150 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 5650 3450 5650
Wire Wire Line
	3450 5550 3450 5650
Connection ~ 3450 5650
Wire Wire Line
	3450 5650 3300 5650
Wire Wire Line
	4150 5650 4150 5600
Wire Wire Line
	3850 5250 4150 5250
Wire Wire Line
	4150 5300 4150 5250
Connection ~ 4150 5250
Wire Wire Line
	4150 5250 5300 5250
Text GLabel 9050 2900 0    50   Input ~ 0
GNDA
Text GLabel 9050 3000 0    50   Input ~ 0
+5VA
Text Notes 8400 3750 0    50   ~ 0
The cable attached to this connector must be short.\nSPI/I2C signals are not meant to drive long buses.
Text Notes 9450 2250 0    50   ~ 0
This is the vacuum tight version\nof the LEMO connector and to be\nmounted in the blind flange.
$EndSCHEMATC
